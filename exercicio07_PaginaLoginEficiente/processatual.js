const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 50 + "px",
            usuario:'',
            senha:'',
            erro: null,
            sucesso: null,

            //arrays (vetores) para armazenamento dos nomes de usuario e senhas
            usuarios: ["admin","caua","souza"],
            senhas: ["1234","1234","1234"],
            userAdmin: false,
            mostrarEntrada: false,
            //variaveis para tratamento das informaçoes dos novos usuarios
            newUsername:"",
            newPasswoed:"",
            confirmPassword:"",
            mostrarLista: false,
        };//fechamento return
    },//fechamento data

    methods:{
        getNumero(numero){
            this.ajusteTamanhoDisplay();
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;

                }
                //this.valorDisplay = this.valorDisplay + numero.toString();
                //adicao simplificada
                this.valorDisplay += numero.toString();
            }
        },//fechamento getNumero

        c(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.tamanhoLetra = 50 + "px";
        },//fechamento c-limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){


                this.valorDisplay += ".";
            }//fechamento if
        },//fechamrnto decimal
        operacoes(operacao){
           
             if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        case "-": this.valorDisplay = (this.numeroAtual - displayAtual). toString();
                            break;
                        
                        case "*":
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;
                        case "/":
                            if(displayAtual == 0 || this.numeroAtual == 0){
                                this.valorDisplay = "Operação impossível!"
                            }
                            else{
                                this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            }
                            break;
                            
                    }//fim do switch
                    
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "="){
                        this.operador = null;
                    }
                   
                    //acertar o numero de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }

                }//fim do if
                else{
                    this.numeroAnterior = displayAtual;

                }//fim do else
            }//fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            this.ajusteTamanhoDisplay();
           
        },
        //fim das operacoes
        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px";
        }
            else if(this.valorDisplay.length >=20){
                this.tamanhoLetra = 20 + "px";
            }
            else if(this.valorDisplay.length >=12){
                this.tamanhoLetra = 30 + "px";
            }
            else{
                this.tamanhoLetra = 50 + "px";
            }
          
        },//fechamento ajuste do display

        login(){
            // alert("testando...")

            //simulando uma requisisão de login assincrona
            setTimeout(() => {
                if((this.usuario === "Caua" && this.senha === "12345678") || 
                (this.usuario === "Souza" && this.senha === "87654321")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    window.location.href='calculadora.html';
                    // alert("Login efetuado com sucesso!");
                }//fim do if
                else{
                    // alert("usuário ou senha incorretos!")
                    this.erro = "Putzz deu ruim! Tente novamente";
                    this.sucesso = null;
                }
            }, 1000);
        },//fechamento login

        login2(){
            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;
                  //veerificação de usuario e senha cadastrados ou não nos arrays
            const index = this.usuarios.indexOf(this.usuario);
            if(index !== -1 && this.senhas[index] === this.senha){
                this.erro = null;
                this.sucesso = "login efetuado com sucesso!";

                localStorage.setItem("usuario",this.usuario);
                localStorage.setItem("senha",this.senha);

                //verificando se o usuario é admin
                if(this.usuario === "admin" && index === 0){
                    this.userAdmin = true;
                    this.sucesso = "logado como ADMIN!";

                }//fechamento if do if
            }//fechamento if
            else{
                this.sucesso = null;
                this.erro = "Usuário e / ou senha incorretos!";

            }
                
            }, 1000);
        },//fechamento login2
        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "carregando página de cadastro...";

                //registrando o usuarioo localStorange para lembrete de acessos
                

                //espera estrategica antes do carregamento da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                    
                }, 1000);

            }//fechamento if
            else{
                this.sucesso = null;
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.erro = "Sem privilégios ADMIN!";
                    
                }, 1000);
            }

        },//fechamento pagina cadastro
        adicionarUsuario(){
            this.mostrarEntrada = false;
            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");            

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.usuario === "admin"){
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== "" && !this.newUsername.includes(" ")){
                        //validacao da senha digitada para cadastro no array
                        if(this.newPasswoed !== "" && !this.newPasswoed.includes(" ") && this.newPasswoed === this.confirmPassword){
                            //inserindo o novo usuario e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPasswoed);
                            //atualizando o usuário recém cadastrado no localStorange
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername = "";
                            this.newPasswoed = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";
                        }//fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "por favor, informe uma senha válida!";
                            
                            this.newUsername = "";
                            this.newPasswoed = "";
                            this.confirmPassword = "";
                        }//fechamento else
                    }//fechamento if includes
                    else{
                        this.erro = "Usuário já cadastrado ou inválido! por favor digite um usuário diferente!";
                        this.sucesso = null;
                        
                        this.newUsername = "";
                        this.newPasswoed = "";
                        this.confirmPassword = "";

                    }//fechamento else              

                }//fechamento if admin
                else{
                    this.erro = "Não está logado como ADMIN!";
                     
                    this.newUsername = "";
                    this.newPasswoed = "";
                    this.confirmPassword = "";

                }//fechamento else admin
            }, 1000);
        },//fechamento adicionarUsuario      this.sucesso = null;
             
        listarUsuarios(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//fechamento listarUsuarios
        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                this.erro = "usuário ADMIN não pode ser excluido!";                    
                }, 500);
                return;//força a saida deste bloco
            }//fechamento if
            if(confirm("tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index != -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    //atualiza o array ususarios no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                    
                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluido com sucesso!";
                    }, 500);
                }//fechamento if index
            }//fechamento if
        },//fechamento excluirUsuario

        


    },//fechamento methods

}).mount("#app");//fechamento app