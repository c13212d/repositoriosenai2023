const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario:'',
            senha:'',
            erro: null,
            sucesso: null,
        }//fechamento return

    },//fechamento data

    methods:{
        login(){
            // alert("testando...")

            //simulando uma requisisão de login assincrona
            setTimeout(() => {
                if((this.usuario === "Caua" && this.senha === "12345678") || 
                (this.usuario === "Souza" && this.senha === "87654321")){
                    this.erro = null;
                    this.sucesso = "Beleuza creuza! :)"
                    // alert("Login efetuado com sucesso!");
                }//fim do if
                else{
                    // alert("usuário ou senha incorretos!")
                    this.erro = "Putzz deu ruim! Tente novamente :(";
                    this.sucesso = null;
                }
            }, 1000);
        },//fechamento login
    },//fechamento methods

}).mount("#app");